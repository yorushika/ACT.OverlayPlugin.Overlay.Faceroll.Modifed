# ACT.OverlayPlugin: Faceroll_Modified Overlay

![pipeline status](https://gitlab.com/yorushika/yorushika.gitlab.io/badges/master/pipeline.svg)

OverlayPlugin 插件用 Faceroll 模版自行修改版。

路径：https://yorushika.gitlab.io/ACT.OverlayPlugin.Overlay.Faceroll.Modifed/

经典版本：https://yorushika.gitlab.io/faceroll_Overheal_CritDirectHitPct/

原作者：[Mandock Saber](https://twitter.com/mandock_saber)

现作者：[Yorushika](mailto:jeremiahshi@outlook.com)

为了我亲爱的夏夏